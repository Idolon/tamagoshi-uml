import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class UseAction implements ActionListener
{
	InteractivePanel interactivePanel;
	Pet pet;
	int idItem;
	
	public UseAction(InteractivePanel interactivePanel, Pet pet, int idItem)
	{
		this.interactivePanel = interactivePanel;
		this.pet = pet;
		this.idItem = idItem;
	}
	/*
	 * (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		interactivePanel.selectItemShop(pet, idItem);
	}
}