/**
 * class Quest
 * @author Loic Vierin
 *
 */
public abstract class Quest {
	private int id ;
	private int progress ;
	private int reward ;
	private String text ;
	
	/**
	 * Constructeur
	 * @param id
	 * @param reward
	 * @param text
	 */
	public Quest(int id, int reward, String text) {
		this.id = id ;
		this.reward = reward ;
		this.text = text ;
	}
	
	public void setProgress (int value) {
		this.progress = value ;
	}
	
	public void setReward (int value) {
		this.reward = value ;
	}
	
	public int getProgress() {
		return this.progress ;
	}
	
	public int getReward() {
		return this.reward ;
	}
}
