import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class UseItemAction implements ActionListener
{
	private InteractivePanel interactivePanel;
	private Pet pet;
	private int idItem;
	
	/**
	 * Construit l'action Listener. On lui sp�cifie le pet qui va utiliser 
	 * un objet ainsi que l'identifiant de cet objet. 
	 * 
	 * @param interactivePanel
	 * @param pet
	 * @param idItem
	 */
	public UseItemAction(InteractivePanel interactivePanel, Pet pet, int idItem)
	{
		this.pet = pet;
		this.idItem = idItem;
		this.interactivePanel = interactivePanel;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	//@Override
	public void actionPerformed(ActionEvent e) {
		pet.useItem(idItem);
		interactivePanel.displayFlashMessage("Item utilis� !");
	}
}