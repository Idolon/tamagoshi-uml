import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe qui permet de g�rer l'action de s�lectionner un item.
 * 
 * @author Mathias
 *
 */
public class SelectItemInventoryAction implements ActionListener
{
	InteractivePanel interactivePanel;
	Pet pet;
	int idItem;
	
	/**
	 * Construit l'action Listener. On lui sp�cifie l'interactive panel
	 * qui va devoir afficher les info de l'item.
	 *
	 * @param interactivePanel 
	 * @param pet
	 * @param idItem
	 */
	public SelectItemInventoryAction(InteractivePanel interactivePanel, Pet pet, int idItem)
	{
		this.interactivePanel = interactivePanel;
		this.pet = pet;
		this.idItem = idItem;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		interactivePanel.selectItemInventory(pet, idItem);
	}
	
}