/**
 * class Attribute
 * @author Loic Vierin
 *
 */
public class Attribute {
	private int id ;
	private String name ;
	private int value ;
	
	/**
	 * Constructeur
	 * @param id
	 * @param name
	 * @param value
	 */
	public Attribute(int id, String name, int value) {
		this.id=id;
		this.name=name;
		this.value=value;
	}
	
	public Attribute(int id) {
		this(id, null, 0);
	}
	
	public int getValue() {
		return value ;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setValue(int value) {
		this.value = value ;
	}
	
	public void display() {
		System.out.println("Attribut : " + name);
		System.out.println("Valeur : " + value);
	}
}
