
public class ItemQuest extends Quest {
	private int idItem ;
	private int number ;
	private int numberTotal ;
	
	public ItemQuest(int id, int reward, String text, int idItem, int numberTotal){
		super(id, reward, text) ;
		this.idItem = idItem ;
		this.numberTotal = numberTotal ;
	}
	
	public void setIdItem(int idItem) {
		this.idItem = idItem ;
	}
	
	public void setNumber(int value) {
		this.number = value ;
	}
	
	public void setNumberTotal(int value) {
		this.numberTotal = value ;
	}
	
	public int getIdItem() {
		return this.idItem ;
	}
	
	public int getNumber() {
		return this.number ;
	}
	
	public int getNumberTotal() {
		return this.numberTotal ;
	}
}
