
import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.Color;
import java.io.File;
import java.io.IOException;

/**
 * C'est le pannel d'interaction, celui o� l'on met toutes les actions possibles.
 * Il contient en fait les barres d'action et le contenu.
 * 
 * @author Mathias
 *
 */
public class InteractivePanel extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BasicMenu basicMenu;
	private ActionBar actionBar;
	private ContentPane contentPane;
	
	
	/**
	 * Permet de construire l'interactive Panel ainsi que ses deux menus,
	 * basicMenu et actionBar.
	 * 
	 * @param width
	 * @param height
	 * @throws IOException
	 */
	public InteractivePanel(int width, int height) throws IOException {
		super();

		// Main features
		this.setLayout(null);
		this.setBackground(null);
		this.setOpaque(false);
		// Build
		build(width, height);
	}
	
	/**
	 * M�thode priv�e qui construit l'InteractivePanel.
	 * 
	 * @param width
	 * @param height
	 * @throws IOException
	 */
	private void build(int width, int height) throws IOException {
        setBackground(Color.BLACK);
        
		// Creating Basic Menu
        buildBasicMenu(width);
		
		// Creating Action Menu 
        buildActionBar(width, height);
        
        // Creating content Pane
        contentPane = new ContentPane(this);
        contentPane.setBounds(0, (int)basicMenu.getBounds().getHeight(), width, height - (int)actionBar.getBounds().getHeight()-60);
        System.out.println("HEIGHT : " + actionBar.getBounds().getHeight());
        System.out.println("Height 2 : " + height);
        contentPane.setBackground(ImageIO.read(new File("res/background_contentPane.png")));
		
		// We add all ! 
		this.add(basicMenu);
		this.add(actionBar);
		this.add(contentPane);

		basicMenu.returnVisible(false);
	}
	
	/**
	 * M�thode priv�e qui construit le basicMenu, le menu du dessus. 
	 * 
	 * @param width
	 * @throws IOException
	 */
	private void buildBasicMenu(int width) throws IOException {
		this.basicMenu = new BasicMenu();
		basicMenu.setBounds(0, 0, width, 38);
		basicMenu.setBackground(ImageIO.read(new File("res/menu/menu_background.png")));
		
		for (final Buttons buttons : Buttons.values()) {
			buttons.createButton(basicMenu, this);
		}
		Buttons.setIcons();

		// Il faut juste finir avec la modification du bouton return.
	}
	
	/**
	 * M�thode priv�e qui construit l'actionBar, menu d'en bas. 
	 * 
	 * @param width
	 * @param height
	 * @throws IOException
	 */
	private void buildActionBar(int width, int height) throws IOException {
		JButton button;
		actionBar = new ActionBar();
		actionBar.setBounds(0, height-56, width, 28);
		actionBar.setBackground(ImageIO.read(new File("res/menu/actionBar_background.png")));
		
		button = new JButton();
		button.setIcon(new ImageIcon("res/menu/icons_actions/eat.png"));
		actionBar.addAction(button);

		button = new JButton();
		button.setIcon(new ImageIcon("res/menu/icons_actions/ball.png"));
		actionBar.addAction(button);		
		
		actionBar.displayActionsPanel();
	}
	
	/**
	 * Permet de r�cup�rer le BasicMenu, menu du haut, de l'InteractivePanel. 
	 * @return
	 */
	public BasicMenu getBasicMenu() {
		return basicMenu;
	}
	
	/**
	 * Affiche les settings
	 */
	public void displaySettings() {
		actionBar.displayActionsPanel();
		basicMenu.returnVisible(true);
		contentPane.displaySettings();
	}
	/**
	 * Affiche l'inventaire. 
	 */
	public void displayInventory() {
		basicMenu.returnVisible(true);
		contentPane.displayInventory();
		actionBar.displayActionsPanel();
	}
	
	/**
	 * Affiche le shop. 
	 */
	public void displayShop() {
		//actionBar.displayBuyPanel();
		basicMenu.returnVisible(true);
		contentPane.displayShop();
		actionBar.displayActionsPanel();
	}
	
	/**
	 * Affiche les qu�tes
	 */
	public void displayQuests()	{
		actionBar.displayActionsPanel();
		basicMenu.returnVisible(true);
		contentPane.displayQuests();
	}
	
	/**
	 * Affiche les achievements. 
	 */
	public void displayAchievements() {
		actionBar.displayActionsPanel();
		basicMenu.returnVisible(true);
		contentPane.displayAchievements();
	}
	
	/**
	 * Revient � la sc�ne et � l'actionBar basique. 
	 */
	public void returnScene() {
		actionBar.displayActionsPanel();
		basicMenu.returnVisible(false);
		contentPane.displayNothing();
	}
	
	/**
	 * Permet de s�lectionner un item et de l'afficher dans l'actionBar
	 * dans un cas d'utilisation.
	 * 
	 * @param pet
	 * @param idItem
	 */
	public void selectItemInventory(Pet pet, int idItem) {
		Item item = pet.getInventory().getItem(idItem);
		actionBar.labelUse(item.getName());
		actionBar.displayUsePanel();
		actionBar.setUseButtonAction(this, pet, idItem);
	}
	
	/**
	 * Permet de s�lectionner un item et de l'afficher dans l'actionBar
	 * dans un cas d'achat.
	 * 
	 * @param pet
	 * @param idItem
	 */
	public void selectItemShop(Pet pet, int idItem)
	{
		Shop shop = pet.getShop();
		Item item = shop.getItem(idItem);
		actionBar.labelBuy(item.getName());
		actionBar.displayBuyPanel();
		actionBar.setBuyButtonAction(this, pet, idItem);
	}
	
	/**
	 * Affiche un message pendant quelques secondes en plein milieu
	 * de l'�cran. 
	 * 
	 * @param message
	 */
	public void displayFlashMessage(String message)
	{
		System.out.println("FLASH MESSAGE : " + message);
	}

}