
import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * C'est la classe qui g�re la sc�ne avec l'animation du pet. 
 * 
 * @author Mathias
 *
 */
public class Scene extends JPanel {
	private Image background;
	private Image pet;
	private int position = 2;
	private int nbPositions = 4;
	private int pet_x = 150;
	private int pet_y = 50;
	private int petSize = 15;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Scene() throws IOException {
		super();
		this.setLayout(null);
		this.setBackground(Color.red);
		build();
	}
	
	private void build() throws IOException {
		pet = ImageIO.read(new File("res/sprites/pet_normal_animation.png"));
	}

	public void setBackground(Image background) {
		this.background = background;
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(background, 0, 0, (int)this.getBounds().getWidth(), (int)this.getBounds().getHeight(), this);
		//g.drawImage(pet, 10, 10, this);
		g.drawImage(pet, pet_x, pet_y, pet_x + petSize, pet_y + petSize, 15 * position, 0, 15 * position + 15, 15, this);
		//g.fillOval(5, 5, 30, 30);
		position = (position + 1) % nbPositions;
	}
}