import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * Enum�ration de tous les boutons du basic menu.
 * 
 * @author Mathias
 */
public enum Buttons implements ActionListener {

	/** **/
	SETTINGS(
			"res/menu/icons/settings.png",
			0, 
			new ActionCallback() {
				@Override
				public void execute(final InteractivePanel panel) {
					panel.displaySettings();
					SETTINGS.getButton().setIcon(new ImageIcon("res/menu/icons/settings_hover.png"));
				}
	}),

	/** **/
	INVENTORY(
			"res/menu/icons/inventoryEmpty.png",
			1, 
			new ActionCallback() {
				@Override
				public void execute(final InteractivePanel panel) {
					panel.displayInventory();
					INVENTORY.getButton().setIcon(new ImageIcon("res/menu/icons/inventoryEmpty_hover.png"));
				}
	}),

	/** **/
	SHOP(
			"res/menu/icons/shop.png",
			2, 
			new ActionCallback() {
				@Override
				public void execute(final InteractivePanel panel) {
					panel.displayShop();
					SHOP.getButton().setIcon(new ImageIcon("res/menu/icons/shop_hover.png"));
				}
	}),

	/** **/
	QUEST(
			"res/menu/icons/quests.png",
			3, 
			new ActionCallback() {
				@Override
				public void execute(final InteractivePanel panel) {
					panel.displayQuests();
					QUEST.getButton().setIcon(new ImageIcon("res/menu/icons/quests_hover.png"));
				}
	}),

	/** **/	
	ACHIEVEMENT(
			"res/menu/icons/achievements.png",
			4, 
			new ActionCallback() {
				@Override
				public void execute(final InteractivePanel panel) {
					panel.displayAchievements();
					ACHIEVEMENT.getButton().setIcon(new ImageIcon("res/menu/icons/achievements_hover.png"));
				}
	}),

	/** **/
	RETURN(
			"res/menu/icons/return.png",
			5, 
			new ActionCallback() {
				@Override
				public void execute(final InteractivePanel panel) {
					panel.returnScene();
				}
	})
	
	;

	/** **/
	private final ImageIcon icon;

	/** **/
	private final JButton delegate;

	/** **/
	private final int position;

	/** **/
	private final ActionCallback callback;

	/** **/
	private InteractivePanel parent;

	/**
	 * Cr�er un bouton. - le JButton n'est alors toujours pas compl�tement cr��.
	 * 
	 * @param path 			de l'image. 
	 * @param position 		de l'image dans le menu. (1, 2, ..., n)
	 * @param callback		fonction � appeler lors de l'appuie.
	 */
	private Buttons(final String path, final int position, final ActionCallback callback) {
		this.icon = new ImageIcon(path);
		this.position = position;
		this.delegate = new JButton();
		this.callback = callback;
		delegate.setVisible(true);
	}

	/**
	 * cr�e le JButton  
	 * 
	 * @param panel
	 * @return
	 */
	public JButton createButton(final JPanel panel, final InteractivePanel parent) {
		this.parent = parent;
		final Insets insets = panel.getInsets();
		delegate.setBounds(
				BasicMenu.offset + BasicMenu.buttonWidth * position + insets.left + BasicMenu.gap * position,
				BasicMenu.margin_top + insets.top,
				BasicMenu.buttonWidth,
				BasicMenu.buttonHeight);
		delegate.setBorderPainted(false);
		delegate.setBackground(null);
		delegate.setOpaque(false);
		delegate.setHorizontalTextPosition(SwingConstants.CENTER);
		panel.add(delegate);
		delegate.addActionListener(this);
		return delegate;
	}

	/**
	 * Permet de r�cup�rer le JButton.
	 * 
	 * @return
	 */
	public JButton getButton() {
		return delegate;
	}

	/**
	 * Permet de r�cup�rer l'icone du JButton. 
	 * @return
	 */
	private ImageIcon getIcon() {
		return icon;
	}

	/*
	 * (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(final ActionEvent e) {
		setIcons();
		callback.execute(parent);
	}

	/**
	 * Permet de modifier l'icone du JButton. 
	 * 
	 */
	public static void setIcons() {
		for (final Buttons button : Buttons.values()) {
			button.getButton().setIcon(button.getIcon());
		}
	}

}
