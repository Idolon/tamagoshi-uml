import java.util.HashMap;

/**
 * class Item
 *
 * @author Loic Vierin
 */
public class Item {

    private int id;
    private String name;
    private String image;
    private int cost;
    //private int [][] attribute ; // ID Valeur
    //private vector < pair  < Integer , Integer > > attribute ;
    //private Vector<Integer> attributeTab; //Attribut ID --> Valeur de modification de l'attribut
    private HashMap<Integer, Integer> attributeTab; // ID - VALEUR DE MODIF

    //private int idNext ;
    private int consumable;
    private int max; //maximum que le pet peut avoir


    /**
     * Constructeur
     *
     * @param id
     * @param name
     * @param image
     * @param cost
     * @param consumable
     * @param max
     */
    public Item(int id, String name, String image, int cost, int consumable, int max) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.cost = cost;
        this.attributeTab = new HashMap<Integer, Integer>();
        //this.idNext=idNext;
        this.consumable = consumable;
        this.max = max;

    }

    public int getConsumable() {

        return consumable;

    }

    public int getId() {
        return id;

    }

    public String getName() { //ajouter
        return name;
    }

    public int getMax() { //ajouter
        return max;
    }

    public int getCost() {
        return cost;
    }


    public void add_Attribute(int at, int val) {

        //this.attributeTab.add(at.getId(), val);
        attributeTab.put(at, val);
    }

	/* CE NEST PAS A ATTRIBUTE MAIS A PET DE FAIRE CA ?
    public void editAttribute ( Pet pet ) { //bolean -> void
		
		for ( int i = 0 ; i < attributeTab.size() ; i++ ) {
			
			pet.setAttribute ( attribute[i][0], attribute[i][1]);
			                                         
		}
	}*/

	
	
	
	
	/*public void DisplayItem ( )	{
		
		
		System.out.println(" ITEM: id= " + id + " name= "+ name);
		System.out.println(" cost= " + cost);
		
		for ( int i = 0 ; i < attribute.length ; i++ ) {
			
			System.out.println("	- " + attribute[i][0] + " : " + attribute[i][1]);
			
		}
		
		
	}*/


    /**
     * DisplayItem: affichage des informations de l'item en console.
     */
    public void DisplayItem() {

        System.out.println(
                " ITEM: id=" + id +
                        " name=" + name +
                        " image=" + image +
                        " cost=" + cost +
                        " consumable=" + consumable +
                        " max=" + max);

        for (int i = 0; i < attributeTab.size() + 10; i++) {

            if (attributeTab.get(i) != null)
                System.out.println("	- Attribut n°" + i + " : " + attributeTab.get(i));

        }


    }

    public HashMap<Integer, Integer> getAttributeTab() {
        return attributeTab;
    }


}
