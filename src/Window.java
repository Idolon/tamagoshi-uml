
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;

import java.awt.*;
import java.io.File;
import java.io.IOException;


/**
 * Classe qui repr�sente notre fen�tre. C'est elle que l'on va cr�er pour 
 * lancer tout le processus. 
 * 
 * @author Mathias
 *
 */
public class Window extends JFrame implements Runnable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Scene scene;  
	private InteractivePanel interactivePanel;
	private int width = 320;
	private int height = 190;
	
	public Window() throws IOException
	{
		super();
		build();
	}

	private void build() throws IOException
	{

		// Caractéristiques de la page.
		setTitle("Tamagotchi");
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
				
		// Constructing interactive panel
		interactivePanel = new InteractivePanel(width, height);
		interactivePanel.setBounds(0, 0, width, height);
		
		// Constructing scene panel
		scene = new Scene();
		scene.setBackground(ImageIO.read(new File("res/scenes/scene1.png")));
		scene.setBounds(0, (int)interactivePanel.getBasicMenu().getBounds().getHeight(), width, 100);

		

		// Size of the window 
		//Insets insets = getInsets(); 
		//setSize(width + insets.left + insets.right,
		//	height + insets.top + insets.bottom);
		setSize(width, height);
		// On récupère le layeredPane.
		JLayeredPane layeredPane = getLayeredPane();

		// On s'occupe du layeredPane
		layeredPane.add(scene, JLayeredPane.DEFAULT_LAYER);
		layeredPane.add(interactivePanel, JLayeredPane.PALETTE_LAYER);
		
		//go();
		Thread t = new Thread(this, "UIRefresh");
		t.setDaemon(true);
		t.start();
	}
	
	public void run()
	{
		for(;;)
		{
			scene.repaint();
			//interactivePanel.update(interactivePanel.getGraphics());
			try {
				Thread.sleep(200);
		    } catch (InterruptedException e) {
		    	e.printStackTrace();
		    }
		}
	}
}