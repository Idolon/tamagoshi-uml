
/**
 * Interface de callback. Elle sert � d�finir l'action que l'on doit r�aliser 
 * pour les boutons par exemple. 
 * 
 * @author Mathias
 */
public interface ActionCallback {

	/**
	 * La fonction qui sera ex�cut� via l'interactive panel.
	 * @param target
	 */
	void execute(final InteractivePanel target);

}
