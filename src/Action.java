import java.util.HashMap;

/**
 * Created by kserino on 09/12/14.
 */
public class Action {
    private int id;
    private String name;
    private String animation;
    private HashMap<Integer, Integer> attributeTab; // ID - VALEUR DE MODIF

    public Action(int id, String name, String animation) {
        this.id = id;
        this.name = name;
        this.animation = animation;
        this.attributeTab = new HashMap<Integer, Integer>();
    }

    public int getId() {
        return id;
    }

    public HashMap<Integer, Integer> getAttributeTab() {
        return attributeTab;
    }

    public void addAttribute(int attributeId, int value) {
        attributeTab.put(attributeId, value);
    }
}