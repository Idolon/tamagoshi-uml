import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import javax.management.monitor.Monitor;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

/**
 * class Pet
 * @author Loic Vierin
 *
 */
public class Pet {
	private String name ;
	private Inventory inventory ;
	private Shop shop ;
	private String image ;
	private String environment ;
	private String gender ;
	private int difficulty ;
	private HashMap < Integer, Attribute > attributeTab ;
	private HashMap < Integer, Quest > questTab ;
	private HashMap < Integer, Action > actionTab ;
	private HashMap < Integer, Achievement > achievementTab ;
	
	/**
	 * Constructeur
	 * @param name
	 * @param image
	 * @param environment
	 * @param gender
	 */
	public Pet(String name, String image, String environment,  String gender, int difficulty ) {
		this.name=name;
		this.image = image ;
		this.environment = environment ;
		this.gender = gender;
		this.difficulty = difficulty ;
		
		this.inventory = new Inventory();
		this.shop = new Shop();

		this.attributeTab = new HashMap < Integer, Attribute > ();
		this.actionTab = new HashMap < Integer, Action > ();

		/*for(int i=0; i<7; i++) {
			attributeTab.add( new Attribute(i) );
		}*/
		
		this.questTab = new HashMap < Integer, Quest>();
		this.achievementTab= new HashMap < Integer, Achievement>();

		parse();
		
		
		
	}
	
	/**
	 * Constructeur 2 sans arguments.
	 */
	public Pet(String xml) {
		this.shop = new Shop();
		this.inventory = new Inventory();
		this.attributeTab = new HashMap < Integer, Attribute > ();

		/*for(int i=0; i<7; i++) {
			attributeTab.add( new Attribute(i) );
		}*/
		
		this.questTab = new HashMap < Integer, Quest>();
		this.achievementTab= new HashMap < Integer, Achievement>();
		this.actionTab= new HashMap < Integer, Action>();

		parse();
		load(xml);
		
	}
	

	/**
	 * addAttribute: Ajout d'un attribut au pet dans son hashmap d'attributs.
	 * @param attribute
	 */
	public void addAttribute ( Attribute attribute)
	{
		attributeTab.put(attribute.getId(), attribute);
	}
	
	
	

	public void setName ( String name ) {
		this.name = name ;
	}
	
	public void setImage ( String image ) {
		this.image = image ;
	}
	
	public void setEnvironment ( String environment ) {
		this.environment = environment ;
	}
	
	public void setGender ( String gender ) {
		this.gender = gender ;
	}
	
	/**
	 * setAttribute: AJOUT a la valeur d'un attribut.
	 * @param idAttribute
	 * @param value
	 */
	public void setAttribute ( int idAttribute, int value ) {
		
		attributeTab.get(idAttribute).setValue(attributeTab.get(idAttribute).getValue()+value);
		
	}
	
	/**
	 * MODIFICATION de la valeur d'un attribut
	 * @param idAttribute
	 * @param value
	 */
	public void setAttribute2 ( int idAttribute, int value ) {
		
		attributeTab.get(idAttribute).setValue(value);
		
	}

	/**
	 * Return
	 *
	 * @return
	 */
	public String getName () {
		return this.name ;
	}

	public String getImage () {
		return this.image ;
	}

	public String getEnvironment () {
		return this.environment ;
	}
	
	public int getDifficulty () {
		return this.difficulty ;
	}

	public HashMap < Integer, Attribute > getAttributeTab () {
		return this.attributeTab ;
	}

	/**
	 *
	 * @param idAttribute
	 * @return
	 */
	public Attribute getAttribute (int idAttribute ) {
		return this.attributeTab.get(idAttribute) ;
	}
	
	public HashMap < Integer, Quest> getQuestTab () {
		return this.questTab ;
	}

	public Quest getQuest (int idQuest ) {
		return this.questTab.get(idQuest) ;
	}
	
	public HashMap < Integer, Achievement> getAchievementTab () {
		return this.achievementTab ;
	}

	public Achievement getAchievement (int idAchievement ) {
		return this.achievementTab.get(idAchievement) ;
	}

	public String getGender () {
		return this.gender ;
	}

	public Inventory getInventory () {
		return this.inventory ;
	}
	
	public Shop getShop () {
		return shop;
	}
	
	public void useAction(int idAction) {
		Action action = actionTab.get(idAction) ;
		HashMap <Integer, Integer> actionAttributeTab = action.getAttributeTab();
		for (int i = 0 ; i < actionAttributeTab.size() ; i++ ) {
			if ( actionAttributeTab.containsKey(i) ) {
				this.setAttribute(i,actionAttributeTab.get(i));
			}
		}
	}

	
	
	
	
	/**
	 * DisplayAttributTab: affichage de la valeur des attributs du pet.
	 */
	public void DisplayAttributeTab ( )	{
						
		System.out.println("ATTRIBUTS DU PET");
		
		for ( int i = 0 ; i <= attributeTab.size() ; i++ ) {
			if(attributeTab.containsKey(i)) {
			System.out.println("	- Attribut " + attributeTab.get(i).getName() + " : " + attributeTab.get(i).getValue());
			}
		}
		
	}
	
	/**
	 * Ajout d'un item a l'inventaire du pet.
	 * @param id
	 * @param nb
	 * @return
	 */
	public boolean buyItem(int id, int nb) {
		for (int i=0; i<nb; i++) 
		{
			if(!inventory.addItem(shop.getItem(id)))
				return false;
		}
		return true;
	}
	
	
	/*public void addItemInventory(Item item) {
		inventory.addItem(item);
	}*/
	
		
	
	/**
	 * Utiliser un item de l'inventaire.
	 * @param id
	 */
	public void useItem(int id) {
		
		Item item = inventory.getItem(id);
		
		if(inventory.useItem(item)) //Utilise l'objet
		{
		
		HashMap <Integer, Integer> at = item.getAttributeTab(); //Modification des attributs
		
		for(int i=0; i<=at.size(); i++) {
			
			if(at.containsKey(i)) {
				
				setAttribute(i, (int) at.get(i));
				
			}
				
		}
		
		System.out.println("Modification des attributs effectuée");
		
		}
		else {
			System.out.println("Attributs non modifiés");
		}
		
	}
	
	/*
	public void buyitem(Item item) {
		HashMap at = item.getAttributeTab();
		
		if(at.containsKey(item.getId()))
		{
			if(inventory.addItem(item))
			{
				
				
			}
		}
		
	}*/
	
	public void DisplayInventory(){
		inventory.DisplayInventory();
	}
	
	public void displayShop(){
		shop.displayShop();
	}
	
	/**
	 * ajoute " +/- nb" coins dans l'inventaire.
	 * @param nb
	 */
	public void addMoney(int nb) {
		
		inventory.addMoney(nb);
		
	}
	
	/**
	 * Parse: appelé dans le constructeur.
	 * Il ajoute:
	 *  -les attributs au pet,
	 *  -les quetes
	 *  -les achievements
	 * 
	 */
	public void parse () {
		String file;
		File fXmlFile;
		DocumentBuilderFactory dbFactory;
		DocumentBuilder dBuilder;
		Document doc;
		
		/********ATTRIBUTES********/
		file="xml/attributes.xml";
		try {
			 
			fXmlFile = new File(file);
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fXmlFile);
		 
			doc.getDocumentElement().normalize();
			
			
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			
			NodeList maListe = doc.getElementsByTagName("attribute"); //On récupère les "item" dans une liste
			
			for (int i = 0; i < maListe.getLength(); i++) { // Parcours de la liste
				
				Node monAttribut = maListe.item(i);
				Element monElement = (Element) monAttribut;
				
				
				//System.out.println("Attribute id="+ monElement.getAttribute("id") + " name=" + monElement.getAttribute("name") );
								
				//CREER L'ATTRIBUT ICI
				Attribute monatt = new Attribute ( Integer.valueOf(monElement.getAttribute("id")), monElement.getAttribute("name"), 50); 
				//AJOUTER L'ATTRIBUT AU PET
				addAttribute(monatt);
				
				
		 	}
		
			//System.out.println("******** fin parseur " + doc.getDocumentElement().getNodeName() + "*********");
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		/** QUESTTAB*/
		file="xml/quests.xml";
		try {
			 
			fXmlFile = new File(file);
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fXmlFile);
		 
			doc.getDocumentElement().normalize();
			
			
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			
			NodeList maListe = doc.getElementsByTagName("actionQuests"); //On récupère les "actionQuests" dans une liste
			
			for (int i = 0; i < maListe.getLength(); i++) { // Parcours de la liste
				
				Node monaction = maListe.item(i);
				Element monElement = (Element) monaction;
				
//				System.out.println(
//					"quest id=" + monElement.getAttribute("id") +
//					" reward= " + monElement.getAttribute("reward") +
//					" idAction= " + monElement.getAttribute("idAction") +
//					" number= " + monElement.getAttribute("number") +
//					" text= " + monElement.getAttribute("text"));
				
				//CREER UNE ACTIONQUEST
				ActionQuest monAQ = new ActionQuest(
					Integer.valueOf(monElement.getAttribute("id")),
					Integer.valueOf(monElement.getAttribute("reward")),
					monElement.getAttribute("text"),
					Integer.valueOf(monElement.getAttribute("idAction")), 
					Integer.valueOf(monElement.getAttribute("number")));
				
			//AJOUT A LA HASHMAP
				questTab.put(Integer.valueOf(monElement.getAttribute("id")), monAQ);
			
			
			
			}
			
			NodeList maListe2 = doc.getElementsByTagName("itemQuests"); //On récupère les "actionQuests" dans une liste
			
			for (int i = 0; i < maListe2.getLength(); i++) { // Parcours de la liste
				
				Node monaction = maListe2.item(i);
				Element monElement = (Element) monaction;
				
//				System.out.println(
//					"quest id=" + monElement.getAttribute("id") +
//					" reward= " + monElement.getAttribute("reward") +
//					" idItem= " + monElement.getAttribute("idItem") +
//					" number= " + monElement.getAttribute("number") +
//					" text= " + monElement.getAttribute("text"));
				

				//CREER UNE ACTIONQUEST
				ItemQuest monIQ = new ItemQuest(
					Integer.valueOf(monElement.getAttribute("id")),
					Integer.valueOf(monElement.getAttribute("reward")),
					monElement.getAttribute("text"),
					Integer.valueOf(monElement.getAttribute("idItem")), 
					Integer.valueOf(monElement.getAttribute("number")));
				
			//AJOUT A LA HASHMAP
				questTab.put(Integer.valueOf(monElement.getAttribute("id")), monIQ);
			
			
			}
			
		//System.out.println("******** fin parseur " + doc.getDocumentElement().getNodeName() + "*********");
		
		} catch (Exception e) {
			e.printStackTrace();
		}
			
			
		/**ACHIEVMENTS**/
		file="xml/achievements.xml";
		try {

			fXmlFile = new File(file);
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();


			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

			NodeList maListe = doc.getElementsByTagName("achievement"); //On récupère les "achievement" dans une liste

			for (int i = 0; i < maListe.getLength(); i++) { // Parcours de la liste

				Node monachievement = maListe.item(i);
				Element monElement = (Element) monachievement;

//				System.out.println(
//					"achievement id=" + monElement.getAttribute("id") +
//					" reward= " + monElement.getAttribute("reward") +
//					" idQuest= " + monElement.getAttribute("idQuest") +
//					" number= " + monElement.getAttribute("number") +
//					" text= " + monElement.getAttribute("text"));

				//CREER UNE ACTIONQUEST
				Achievement monA = new Achievement(
					Integer.valueOf(monElement.getAttribute("id")),
					Integer.valueOf(monElement.getAttribute("reward")),
					Integer.valueOf(monElement.getAttribute("idQuest")),
					Integer.valueOf(monElement.getAttribute("number")),
					monElement.getAttribute("text"));

			//AJOUT A LA HASHMAP
				achievementTab.put(Integer.valueOf(monElement.getAttribute("id")), monA);



			}

			//System.out.println("******** fin parseur " + doc.getDocumentElement().getNodeName() + "*********");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		/**ACTIONS**/
		file="xml/actions.xml";
		try {

			fXmlFile = new File(file);
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fXmlFile);

			doc.getDocumentElement().normalize();


			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

			NodeList maListe = doc.getElementsByTagName("action"); //On récupère les "action" dans une liste

			for (int i = 0 ; i < maListe.getLength() ; i++) { // Parcours de la liste

				Node monAction = maListe.item(i);
				Element monElement = (Element) monAction;


				//CREER L'ACTION ICI
				Action action = new Action(Integer.valueOf(monElement.getAttribute("id")), monElement.getAttribute("name"), monElement.getAttribute("animation"));

				NodeList listeAttribute = monAction.getChildNodes(); //on récupère la liste d'attributs d'une action

				for (int j = 1; j < listeAttribute.getLength(); j+=2) {


					Node monAtt = listeAttribute.item(j);
					Element monElement2 = (Element) monAtt;

					if(listeAttribute.item(j).getNodeName()=="attribute") {


						//AJOUTER LES ATTRIBUTS A L'ACTION ICI avec addAttribute
						//System.out.println(Integer.valueOf(monElement2.getAttribute("value")));
						action.addAttribute(Integer.valueOf(monElement2.getAttribute("id")), Integer.valueOf(monElement2.getAttribute("value")));
					}


				}

				//AJOUTER L'ACTION A LA STRUCTURE PREVUE A CETTE EFFET
				//comme pour item on avait fait:
				actionTab.put(action.getId(), action);
			}

			//System.out.println("******** fin parseur " + doc.getDocumentElement().getNodeName() + "*********");

		} catch (Exception e) {
			e.printStackTrace();
		}
			
	}

	/**
	 * save: Sauvegarde
	 */
	public void save(String xml) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();	
		try {
		    DocumentBuilder builder = factory.newDocumentBuilder();
		    Document doc= builder.newDocument();
		  
		    Element save = doc.createElement("save");
		    doc.appendChild(save);			
		    
		    Element attributes = doc.createElement("attributes");
		    HashMap <Integer,Attribute> tab = this.getAttributeTab() ;
		    for ( int i = 0 ; i < tab.size() ; i++ ) {
			    Element attribute = doc.createElement("attribute");
			    attribute.setAttribute("id", String.valueOf(tab.get(i).getId()));
			    attribute.setAttribute("name", tab.get(i).getName());
				attribute.setAttribute("value", String.valueOf(tab.get(i).getValue()));
			    attributes.appendChild(attribute);
		    }
		    save.appendChild(attributes);
		    
		    Element inventory = doc.createElement("inventory");
		    HashMap <Integer, Item> items = this.getInventory().getItemId();
		    HashMap <Integer, Integer> itemsNumber = this.getInventory().getItemNumber();
		    for(int i=0; i<=items.size()+1000; i++) {
				if(items.containsKey(i)) {
				    Element item = doc.createElement("item");
					item.setAttribute("id", String.valueOf(items.get(i).getId()));
					item.setAttribute("name", items.get(i).getName());
					item.setAttribute("number", String.valueOf(itemsNumber.get(i)));
				    inventory.appendChild(item);
				}
			}
		    save.appendChild(inventory);
		    
			Date now = new Date();
		    String timestamp = String.valueOf(now.getTime()) ;
		    Element time = doc.createElement("time") ;
		    time.setAttribute("value", timestamp) ;
		    save.appendChild(time) ;
		    
		    Element options = doc.createElement("options") ;
		    options.setAttribute("name", this.getImage()) ;
		    options.setAttribute("gender", this.getGender()) ;
		    options.setAttribute("image", this.getImage()) ;
		    options.setAttribute("environment", this.getEnvironment()) ;
		    options.setAttribute("difficulty", String.valueOf(this.getDifficulty())) ;
		    save.appendChild(options) ;

		    TransformerFactory transformerFactory = TransformerFactory.newInstance();
		    Transformer transformer = transformerFactory.newTransformer();
		    DOMSource source = new DOMSource(doc);
		    StreamResult out = new StreamResult(new File(xml));
		    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		    transformer.transform(source, out);
		}
		catch (ParserConfigurationException e) {
		    e.printStackTrace();
		}
		catch (TransformerConfigurationException e) {
		    e.printStackTrace();
		}
		catch (TransformerException e) {
		    e.printStackTrace();
		}
	}
	

    public void load(String xml) {

		File fXmlFile;
		DocumentBuilderFactory dbFactory;
		DocumentBuilder dBuilder;
		Document doc;
		try {
			fXmlFile = new File(xml);
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			
			
			NodeList attributes = doc.getElementsByTagName("attribute");
			for (int i = 0 ; i < attributes.getLength() ; i++) { 
				Element attribute = (Element) attributes.item(i);
				this.setAttribute2(Integer.valueOf(attribute.getAttribute("id")), Integer.valueOf(attribute.getAttribute("value"))) ;
				//System.out.println(attribute.getAttribute("id") + " : " + attribute.getAttribute("name") + " : " + attribute.getAttribute("value"));
			}
			
			NodeList inventory = doc.getElementsByTagName("item");
			for (int i = 0 ; i < inventory.getLength() ; i++) { 
				Element item = (Element) inventory.item(i);
				if ( Integer.valueOf(item.getAttribute("id")) == 0 ) {
					this.inventory.addMoney(Integer.valueOf(item.getAttribute("number"))) ;
				} else {
					this.inventory.addMoney(this.shop.getItem(Integer.valueOf(item.getAttribute("id"))).getCost()*Integer.valueOf(item.getAttribute("number"))) ;
					this.buyItem(Integer.valueOf(item.getAttribute("id")), Integer.valueOf(item.getAttribute("number"))) ;
				}
				//System.out.println(item.getAttribute("id") + " : " + item.getAttribute("name") + " : " + item.getAttribute("number"));
			}
			
			NodeList options = doc.getElementsByTagName("options");
			Element option = (Element) options.item(0) ;
			this.name = option.getAttribute("name") ;
			this.image = option.getAttribute("image") ;
			this.gender = option.getAttribute("gender") ;
			this.environment = option.getAttribute("environment") ;
			this.difficulty = Integer.valueOf(option.getAttribute("difficulty")) ;
		    //System.out.println(option.getAttribute("difficulty"));
			
			NodeList timeNode = doc.getElementsByTagName("time") ;
			Element time = (Element) timeNode.item(0) ;
			Date now = new Date();
			
			//long timeSpent = now.getTime() - Long.valueOf(time.getAttribute("value"));
			long timeSpent = now.getTime() - Long.valueOf(time.getAttribute("value"));
			//System.out.println(timeSpent/1000);
			//this.spentTime(timeSpent/1000);
			//il faut utiliser la fonction spentTime(int sec) pour lui dire combien de temps elle doit faire passer pour le tama
			//Cette fonction doit récupérer dans les options la difficulté et modifier le pet en fonction
			System.out.println("Welcome back ! You have been away for "+ timeSpent/1000 +" sec ");
        }
        catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        catch (SAXException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }	
    }
}
