
public class ActionQuest extends Quest {
	private int idAction ;
	private int number ;
	private int numberTotal ;
	
	public ActionQuest(int id, int reward, String text, int idAction, int numberTotal){
		super(id, reward, text) ;
		this.idAction = idAction ;
		this.numberTotal = numberTotal ;
	}
	
	public void setIdAction(int idAction) {
		this.idAction = idAction ;
	}
	
	public void setNumber(int value) {
		this.number = value ;
	}
	
	public void setNumberTotal(int value) {
		this.numberTotal = value ;
	}
	
	public int getIdAction() {
		return this.idAction ;
	}
	
	public int getNumber() {
		return this.number ;
	}
	
	public int getNumberTotal() {
		return this.numberTotal ;
	}
}
