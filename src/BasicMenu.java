//import java.awt.Dimension;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.JPanel;

/**
 * Ce menu basique est celui qui contient le bouton des settings 
 * de l'inventaire, des qu�tes etc ... Il se trouve en haut de 
 * l'interface. 
 * 
 * @author Mathias
 *
 */
public class BasicMenu extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Image background;
	
	public static final int gap = 5; 
	public static final int buttonWidth = 32; 
	public static final int buttonHeight = 32;
	public static final int offset = 10;
	public static final int margin_top = 3;

	/**
	 * Construction du basicMenu. 
	 */
	public BasicMenu() {
		this.setLayout(null);
		this.setBackground(Color.black);
		this.setOpaque(true);
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.awt.Component#setBounds(int, int, int, int)
	 */
	@Override
	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);
		Insets insets = getInsets(); 						// Get Inset
		Buttons.RETURN.getButton().setBounds(width - offset, margin_top + insets.top,
		buttonWidth, buttonHeight);							// Set position 
	}

	/**
	 * Permet de d�finir l'image qui set de background au menu.
	 * Cette image est r�p�t�e sur l'axe des x et des y.
	 * 
	 * @param background
	 */
	public void setBackground(Image background) {
		this.background = background;
	}

	/**
	 * Permet de d�finir si le bouton "return" est visible ou non.
	 * 
	 * @param b
	 */
	public void returnVisible(boolean b) {
		Buttons.RETURN.getButton().setVisible(b);
	}
	
	/*
	 * (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        int iw = background.getWidth(this);
        int ih = background.getHeight(this);
        if (iw > 0 && ih > 0) {
            for (int x = 0; x < getWidth(); x += iw) {
                for (int y = 0; y < getHeight(); y += ih) {
                    g.drawImage(background, x, y, iw, ih, this);
                }
            }
        }
    } 
}
