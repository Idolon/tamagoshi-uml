import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.util.HashMap;

import javax.swing.*;


/**
 * Cette classe contient diff�rents panneaux comme celui de l'inventaire , les
 * qu�tes, le shop etc ... Elle permettra de switcher facilement entre chacun de
 * ces panneaux.
 * 
 * @author Mathias
 *
 */
class ContentPane extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Image background = null;
	Image activeBackground = null;
	int offset = 5;

	private JComponent settingsPanel, inventoryPanel, shopPanel, questsPanel,
			achievementsPanel;
	JScrollPane scroller;
	InteractivePanel interactivePanel;

	/**
	 * Initialise tout le content pane en cr�ant les diff�rents panels possible
	 * : Settings, Inventory, Shop, Quests et Achivement.
	 */
	public ContentPane(InteractivePanel interactivePanel) {
		this.interactivePanel = interactivePanel;
		setLayout(new BorderLayout());
		setOpaque(false);
		buildSettingsPanel();
		buildInventoryPanel();
		buildShopPanel();
		buildQuestsPanel();
		buildAchievementsPanel();
		

		add(settingsPanel);
		add(inventoryPanel);
		add(shopPanel);
		add(questsPanel);
		add(achievementsPanel);
		displayNothing();
	}

	/**
	 * Initialise le panel settings.
	 */
	private void buildSettingsPanel() {
		settingsPanel = new JPanel();
		settingsPanel.setOpaque(false);
		JButton button = new JButton("En construction !");
		button.setBounds(0, 0, 100, 100);
		settingsPanel.add(button);
	}

	/**
	 * Initialise le panel inventory
	 */
	private void buildInventoryPanel() {
		inventoryPanel = new JPanel();
		inventoryPanel.setOpaque(false);
		setInventoryPanel();
	}

	private void setInventoryPanel() {
		Pet pet = Engine.getPet();
		Inventory inventory = pet.getInventory();
		HashMap<Integer, Item> items = inventory.getItemId();
		HashMap<Integer, Integer> items_number = inventory.getItemNumber();

		JButton button;
		for (int i = 0; i <= items.size() + 1000; i++) {
			if (items.containsKey(i)) {
				button = new JButton(items.get(i).getName() + " | "
						+ items_number.get(i));
				button.addActionListener(new SelectItemInventoryAction(
						interactivePanel, pet, i));
				inventoryPanel.add(button);
			}
		}
	}

	/**
	 * Initialise le panel shop.
	 */
	private void buildShopPanel() {

		JPanel shopContent = new JPanel();
		shopPanel = new JScrollPane();
		((JScrollPane)shopPanel).setViewportView(shopContent);
		shopPanel.setOpaque(false);
		shopPanel.setBackground(null);

		
		// Shop Content 
		shopContent.setPreferredSize(new Dimension(100, 200));
		shopContent.setOpaque(false);
		shopContent.setLayout(new FlowLayout());
		shopContent.setBackground(null);
		
		shopPanel.setOpaque(false);
		
		Pet pet = Engine.getPet();
		Shop shop = pet.getShop();
		HashMap<Integer, Item> items = shop.getItemId();

		JButton button;
		for (int i = 0; i <= items.size() + 1000; i++) {
			if (items.containsKey(i)) {
				button = new JButton(items.get(i).getName() + " | " + items.get(i).getCost());
				button.addActionListener(new SelectItemShopAction(
						interactivePanel, pet, i));
				shopContent.add(button);
			}
		}
		
	}

	/**
	 * Initialise le panel quests.
	 */
	private void buildQuestsPanel() {
		questsPanel = new JPanel();
		questsPanel.setOpaque(false);
	}

	/**
	 * Initialise le panel achievements
	 */
	private void buildAchievementsPanel() {
		achievementsPanel = new JPanel();
		achievementsPanel.setOpaque(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.Component#setBounds(int, int, int, int)
	 */
	@Override
	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);
		settingsPanel.setBounds(0, 0, width, height);
		inventoryPanel.setBounds(0, 0, width, height);
		shopPanel.setBounds(0, 0, width, height);
		questsPanel.setBounds(0, 0, width, height);
		achievementsPanel.setBounds(0, 0, width, height);
		setPreferredSize(new Dimension(width, height));
	}

	/**
	 * Affiche le contenu du panel settings.
	 */
	public void displaySettings() {
		activeBackground = background;
		settingsPanel.setVisible(true);
		inventoryPanel.setVisible(false);
		shopPanel.setVisible(false);
		questsPanel.setVisible(false);
		achievementsPanel.setVisible(false);
	}

	/**
	 * Affiche le contenu du panel inventory.
	 */
	public void displayInventory() {
		activeBackground = background;
		remove(inventoryPanel);
		buildInventoryPanel();
		add(inventoryPanel);
		setBounds(getBounds());
		settingsPanel.setVisible(false);
		inventoryPanel.setVisible(true);
		shopPanel.setVisible(false);
		questsPanel.setVisible(false);
		achievementsPanel.setVisible(false);
	}

	/**
	 * Affiche le contenu du panel shop
	 */
	public void displayShop() {
		activeBackground = background;
		settingsPanel.setVisible(false);
		inventoryPanel.setVisible(false);
		shopPanel.setVisible(true);
		questsPanel.setVisible(false);
		achievementsPanel.setVisible(false);

	}

	/**
	 * Affiche le contenu du panel quests
	 */
	public void displayQuests() {
		activeBackground = background;
		settingsPanel.setVisible(false);
		inventoryPanel.setVisible(false);
		shopPanel.setVisible(false);
		questsPanel.setVisible(true);
		achievementsPanel.setVisible(false);
	}

	/**
	 * Affiche le contenu du panel achievements.
	 */
	public void displayAchievements() {
		activeBackground = background;
		settingsPanel.setVisible(false);
		inventoryPanel.setVisible(false);
		shopPanel.setVisible(false);
		questsPanel.setVisible(false);
		achievementsPanel.setVisible(true);
	}

	/**
	 * Affiche aucun panel.
	 */
	public void displayNothing() {
		activeBackground = null;
		settingsPanel.setVisible(false);
		inventoryPanel.setVisible(false);
		shopPanel.setVisible(false);
		questsPanel.setVisible(false);
		achievementsPanel.setVisible(false);
	}

	public void setBackground(Image background) {
		this.background = background;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	public void paintComponent(Graphics g) {
		if (activeBackground == null)
			return;
		super.paintComponent(g);
		int iw = activeBackground.getWidth(this);
		int ih = activeBackground.getHeight(this);
		if (iw > 0 && ih > 0) {
			for (int x = 0; x < getWidth(); x += iw) {
				for (int y = 0; y < getHeight(); y += ih) {
					g.drawImage(activeBackground, x, y, iw, ih, this);
				}
			}
		}
	}

}
