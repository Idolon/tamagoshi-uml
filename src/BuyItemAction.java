import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class BuyItemAction implements ActionListener
{
	int idItem;
	Pet pet;
	InteractivePanel interactivePanel;
	
	public BuyItemAction(InteractivePanel interactivePanel, Pet pet, int idItem)
	{
		this.idItem = idItem;
		this.pet = pet;
		this.interactivePanel = interactivePanel;
	}
	/*
	 * (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		System.out.println("On ach�te une fois !");
		if(pet.buyItem(idItem, 1))
			interactivePanel.displayFlashMessage("Item achet� !");
		else 
			interactivePanel.displayFlashMessage("Pas assez d'argent ou de place !");
	}
}