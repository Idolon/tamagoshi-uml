
import java.util.HashMap;
import java.util.Vector;

import org.w3c.dom.*;

import javax.xml.parsers.*;

import java.io.File;

/**
 * class Shop
 * @author Loic Vierin
 *
 */
public class Shop {


	private HashMap < Integer, Item > itemId;
	
	/**
	 * Constructeur
	 */
	public Shop() {
		// appeler fonction parse
		this.itemId = new HashMap < Integer, Item >();
		parse();
	}
	
	/**
	 * Parse: permet de remplir le shop
	 */
	public void parse () {
		String file;
		File fXmlFile;
		DocumentBuilderFactory dbFactory;
		DocumentBuilder dBuilder;
		Document doc;
		
		file="xml/items.xml";
		try {
			 
			fXmlFile = new File(file);
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(fXmlFile);
		 
			doc.getDocumentElement().normalize();
			
			
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
		
			NodeList maListe = doc.getElementsByTagName("item"); //On récupère les "item" dans une liste
			
			for (int i = 0; i < maListe.getLength(); i++) { // Parcours de la liste
				
				Node monItem = maListe.item(i);
				Element monElement = (Element) monItem;
				
				/*System.out.println(
					"Item id="+ monElement.getAttribute("id") +
					" name=" + monElement.getAttribute("name") +
					" image=" + monElement.getAttribute("image") +
					" cost=" + monElement.getAttribute("cost") +
					" consumable=" + monElement.getAttribute("consumable") +
					" max=" + monElement.getAttribute("max"));*/
				
				//CREER L'iTEM ICI
				
				
				Item item= new Item(
					Integer.valueOf(monElement.getAttribute("id")),
					monElement.getAttribute("name"),
					monElement.getAttribute("image"),
					Integer.valueOf(monElement.getAttribute("cost")),
					Integer.valueOf(monElement.getAttribute("consumable")),
					Integer.valueOf(monElement.getAttribute("max")));
				
				
				NodeList listeAttribute = monItem.getChildNodes(); //on récupère la liste d'attributs d'un item
								
				for (int j = 1; j < listeAttribute.getLength(); j+=2) {
					
					
					Node monAtt = listeAttribute.item(j);
					Element monElement2 = (Element) monAtt;
					
					if(listeAttribute.item(j).getNodeName()=="attribute") {
						
						//System.out.println(" - Attribute id= "+ monElement2.getAttribute("id") + " value= " + monElement2.getAttribute("value"));
						
						//AJOUTER LES ATTRIBUTS A L'ITEM
						item.add_Attribute(Integer.valueOf(monElement2.getAttribute("id")), Integer.valueOf(monElement2.getAttribute("value")));
					}
		
					
				}
				
				//item.DisplayItem();
				itemId.put(item.getId(), item);
				
				
				
				
				
		 	}
		
			//System.out.println("******** fin parseur " + doc.getDocumentElement().getNodeName() + "*********");
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		 
		
		
		
	}
	
	
	public HashMap<Integer, Item> getItemId ( ) {
		
		return itemId;

	}
	
	
	/**
	 * displayShop: affichage console de shop
	 */
	public void displayShop() {
		System.out.println("***DISPLAY SHOP****");
		//TODO: CHercher la key max pour la taille 
		for(int i=0; i<=itemId.size()+1000; i++) {
			
			if(itemId.containsKey(i)) {
				
				//System.out.println("Objet n°"+i+": "+itemId.get(i).getName());
				itemId.get(i).DisplayItem();
				
			}
		}
		System.out.println("***FIN SHOP***");
	}
	
	
	public Item getItem(int id)
	{
		return itemId.get(id);
	}
		
	
	
	
	

	
	
}
