
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.*;

/**
 * Classe de la barre d'action se trouvant en bas de l'interface. 
 * 
 * @author Mathias
 *
 */
class ActionBar extends JPanel {

	/**
	 * Serial auto.
	 */
	private static final long serialVersionUID = 1L;
	// Parameters
	int offset = 10,
	    buttonWidth = 16,
	    buttonHeight = 16,
	    gap = 2,
	    margin_top = 4;
	
	// JPanel
	JPanel actions_p, buy_p, use_p;
	
	// Actions
	Vector<JButton> actions_b;
	
	// Buy info
	JLabel buyLabel;
	JButton buyButton;
	ActionListener buyListener = null;
	
	// Use info
	JLabel useLabel;
	JButton useButton;
	ActionListener useListener = null;
	
	// Images
	Image actions, background;
	
	public ActionBar() {
		this.setLayout(null);
		this.setOpaque(true);
		actions_b = new Vector<JButton>();
		
		buildActionsPanel();
		buildBuyPanel();
		buildUsePanel();
	}
	
	/**
	 * Cr�er panel des actions du jeu.
	 */
	private void buildActionsPanel() {
		actions_p = new JPanel();
		actions_p.setLayout(null);
		actions_p.setVisible(false);
		actions_p.setOpaque(false);
		actions_p.setBounds(0, 0, (int)getBounds().getWidth(), (int)getBounds().getHeight());
		add(actions_p);
	}
	
	/**
	 * Cr�er le panel d'actions d'achat. 
	 */
	private void buildBuyPanel() {
		// Buying Panel
		buy_p = new JPanel();
		buy_p.setLayout(null);
		buy_p.setVisible(false);
		buy_p.setOpaque(false);
		buy_p.setBounds(0, 0, (int)getBounds().getWidth(), (int)getBounds().getHeight());
		// Label
		buyLabel = new JLabel("Buy a lot of things that i want !");
		buyLabel.setBounds(0, 0, 100, 16);
		buy_p.add(buyLabel);
		// Button buy
		buyButton = new JButton("Buy");
		buyButton.setFont(new Font("Arial", Font.PLAIN, 10));
		buyButton.setSize( buyButton.getPreferredSize() );
		buy_p.add(buyButton);
		
		this.add(buy_p);
	}
	
	/**
	 * Cr�er le panel d'actions d'utilisation.
	 */
	private void buildUsePanel() {
		// Using Panel
		use_p = new JPanel();
		use_p.setLayout(null);
		use_p.setVisible(true);
		use_p.setOpaque(false);
		use_p.setBounds(0, 0, (int)getBounds().getWidth(), (int)getBounds().getHeight());
		// Label
		useLabel = new JLabel("Item's label which you want to use.");
		useLabel.setBounds(0, 0, 100, 16);
		use_p.add(useLabel);
		// Button Use
		useButton = new JButton("Use");
		useButton.setFont(new Font("Arial", Font.PLAIN, 10));
		use_p.add(useButton);
		
		this.add(use_p);
	}
	
	/**
	 * Affiche le panel des actions.
	 */
	public void displayActionsPanel() {
		actions_p.setVisible(true);
		use_p.setVisible(false);
		buy_p.setVisible(false);
	}
	
	/**
	 * Affiche le panel d'utilisateur.
	 */
	public void displayUsePanel() {
		actions_p.setVisible(false);
		use_p.setVisible(true);
		buy_p.setVisible(false);
	}
	
	/**
	 * Affiche le panel d'achat.
	 */
	public void displayBuyPanel() {
		actions_p.setVisible(false);
		use_p.setVisible(false);
		buy_p.setVisible(true);
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see java.awt.Component#setBounds(int, int, int, int)
	 */
	@Override
	public void setBounds(int x, int y, int width, int height) {
		super.setBounds(x, y, width, height);
		actions_p.setBounds(0, 0, width, height);
		buy_p.setBounds(0, 0, width, height);
		use_p.setBounds(0, 0, width, height);
		buyButton.setBounds((int)(this.getBounds().getWidth() - buyButton.getPreferredSize().getWidth()) - offset,
				0, (int)buyButton.getPreferredSize().getWidth(), (int)buyButton.getPreferredSize().getHeight());
		useButton.setBounds((int)(this.getBounds().getWidth() - useButton.getPreferredSize().getWidth()) - offset,
				0, (int)useButton.getPreferredSize().getWidth(), (int)useButton.getPreferredSize().getHeight());
	}
	
	/**
	 * Permet d'ajouter une action.
	 * 
	 * @param action
	 * @return
	 */
	public JButton addAction(JButton action) {
		int n = actions_b.size();
		//System.out.println("Add Action " + n);
		Insets insets = getInsets();
		action.setBounds(offset + insets.left + n * buttonWidth + gap * n, margin_top + insets.top,
				buttonWidth, buttonHeight);
		

		action.setBorderPainted(false);
		action.setBackground(null);
		action.setOpaque(true);
		actions_b.addElement(action);
		actions_p.add(action);
		return action;
	}
	
	/**
	 * Modifie ce qui est �crit dans le panel d'achat. 
	 * 
	 * @param text
	 */
	public void labelBuy(String text) {
		buyLabel.setText(text);
	}
	
	/**
	 * Modifie ce qui est �crit dans le panel d'utilisation.
	 * 
	 * @param text
	 */
	public void labelUse(String text) {
		useLabel.setText(text);
	}
	
	/**
	 * Modifie l'action du bouton "use" afin d'appeler une fonction 
	 * qui r�alise l'utilisation.
	 * 
	 * @param pet
	 * @param idItem
	 */
	public void setUseButtonAction(InteractivePanel interactivePanel, Pet pet, int idItem) {
		if(useListener != null)
			useButton.removeActionListener(useListener);
		useListener = new UseItemAction(interactivePanel, pet, idItem);
		useButton.addActionListener(useListener);
	}
	
	
	/**
	 * Modifie l'action du bouton "buy" afin d'appeler une fonction
	 * qui r�alise l'achat. 
	 * 
	 * @param interactivePanel
	 * @param pet
	 * @param idItem
	 */
	public void setBuyButtonAction(InteractivePanel interactivePanel, Pet pet, int idItem) {
		if(buyListener != null)
			buyButton.removeActionListener(buyListener);
		buyListener = new BuyItemAction(interactivePanel, pet, idItem);
		buyButton.addActionListener(buyListener);
	}
	
	/**
	 * Modifie le fond r�p�t� du panel.
	 *  
	 * @param background
	 */
	public void setBackground(Image background)	{
		this.background = background;
	}
	
	/**
	 * Modifie l'image des actions. 
	 * 
	 * @param actions
	 */
	public void setActionsImages(Image actions) {
		this.actions = actions;
	}
	
	/*
	 * (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override	
	public void paintComponent(Graphics g) {
        super.paintComponent(g);
		int iw = background.getWidth(this);
        int ih = background.getHeight(this);
        if (iw > 0 && ih > 0) {
            for (int x = 0; x < getWidth(); x += iw) {
                for (int y = 0; y < getHeight(); y += ih) {
                    g.drawImage(background, x, y, iw, ih, this);
                }
            }
        }
    } 
}