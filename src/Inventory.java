
import java.util.HashMap;
import java.util.Vector;

/**
 * Class Inventory
 * @author Loic Vierin
 *
 */
public class Inventory {

	private HashMap < Integer, Item > itemId;
	private HashMap < Integer, Integer > itemNumber;
	
	/**
	 * Constructeur.
	 * Il instancie les hashmap et il ajoute l'item "coins" en id=0
	 */
	public Inventory( ) {
		this.itemId = new HashMap < Integer, Item >();
		this.itemNumber = new HashMap < Integer, Integer >();
		
		//FAIRE AUTREMENT AVEC L'ENGINE
		
		Item coins= new Item(0, "coins", "noimage", 0, 0, 999999999);
		itemId.put(0,coins);
		itemNumber.put(0, 0);
		
		
	}
	
	public void clearInventory ( ) {
		itemId.clear();
		itemNumber.clear();
	}
	
	
	public HashMap<Integer, Item> getItemId ( ) {
		
		return itemId;

	}
	
	
	public HashMap<Integer, Integer> getItemNumber ( ) {
		
		return itemNumber;

	}
	
	/*
	public Integer getNbItem ( Item item ) //Retourne le nombre de cet objet; si c'est 0, objet pas dans l'inventaire
	{
		
		if( itemId.contains(item) ) {
			
			return itemNumber.get( itemId.indexOf(item) ) ;
			
		}
		
		
		else
			return 0;		
	}*/
	
	public  void addMoney (int nb) {
		
		int val=itemNumber.get(0);
		itemNumber.remove(0);
		itemNumber.put(0, val+nb);
		
	}
	
/**
 * addItem: Ajoute un item dans l'inventaire.
 * Il verifie:
 * 		-si on a assez d'argent
 * 		-qu'on ne dépasse pas le maximum portable
 * @param item
 * @return boolean
 */
public boolean addItem ( Item item ){
		
		if(itemNumber.get(0)>= item.getCost())
		{
			if( itemId.containsValue(item) ) { //Si l'objet est dans l'inventaire
				
				if( itemNumber.get(item.getId()) < item.getMax() ) {// Et qu'on en a moins que le maximum portable
				
					int val=itemNumber.get(item.getId());
					itemNumber.remove(item.getId());
					itemNumber.put(item.getId(), val+1);
					}
				else {
					System.out.println("Problème: Maximum pour '"+ item.getName() +"' Atteint");
					return false;
				}
				
			}
			
			else { // Sinon on ajoute cet objet aux vector
				
				itemId.put(item.getId(), item);
				itemNumber.put(item.getId(),1);
			}
			
			//System.out.println("Ajout de l'item '"+ item.getName()+"' effectuée");
			
			int val=itemNumber.get(0);
			itemNumber.remove(0);
			itemNumber.put(0, val-item.getCost());
			
			return true;
		}
		else
		{
			System.out.println("Vous n'avez pas assez d'argent pour '" + item.getName()+ "' !");
			return false;
		}
	}
	
/**
 * useItem: utiliser un item.
 * On vérifie: si l'item est bien dans l'inventaire.
 * Si il est consommable (ex: une pomme, un ballon): Tire un random avec le getConsumable de l'item.
 * Réduit cet item de l'inventaire.
 * @param item
 * @return
 */
	public boolean useItem ( Item item)
	{

		if( itemId.containsValue(item) ) { //Si l'objet est dans l'inventaire
				
					
			if ( item.getConsumable() >= 0 ) { //et si il est consommable
		
				int rand = 1 + (int)( Math.random() * ( ( 100 - 1 ) + 1 ) ); //random entre 1-100
			
				if( item.getConsumable() >= rand ) {
				
					System.out.println("Objet '" + item.getName() + "' Consommé !");
					
					if( itemNumber.get(item.getId()) == 1   ) {// Si il n'en restait plus qu'un on les supprime des deux vecteurs
						
						itemId.remove(item.getId());
						itemNumber.remove( item.getId());
					}
					else //Si il en restait >1
					{
						int val=itemNumber.get(item.getId());
						itemNumber.remove(item.getId());
						itemNumber.put(item.getId(), val-1);
					}
				
				}
				
				return true;
			
			}
			System.out.println("Objet '"+item.getName()+"' non consommable donc non supprimé");
			return true;
		}
		else {

			System.out.println("Problème: Objet pas dans l'inventaire"); //Ici l'objet item en argument vaut NULL
			return false;
		}
			
	}	
	
	
	/**
	 * DisplayInventory: affichage en console de l'inventaire.
	 */
	public void DisplayInventory() {
		System.out.println("***DISPLAY INVENTORY***");
		//TODO: CHercher la key max pour la taille 
		for(int i=0; i<=itemId.size()+1000; i++) {
			
			if(itemId.containsKey(i)) {
				
				System.out.println("Objet n°"+i+": "+itemId.get(i).getName()+"  x"+itemNumber.get(i));
				
			}
		}
		System.out.println("***FIN INVENTORY***");
		
		
	}
	
	
	public Item getItem(int id)
	{
		return itemId.get(id);
		
	}
	
	
			
	
		
}
