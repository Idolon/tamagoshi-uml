/**
 * Achievements
 * @author Loic Vierin
 *
 */
public class Achievement {
	private int id ;
	private int idQuest ;
	private int reward ;
	private int numberTotal ;
	private String text ;
	
	/**
	 * Constructeur
	 * @param id
	 * @param idQuest
	 * @param reward
	 * @param numberTotal
	 * @param text
	 */
	public Achievement(int id, int idQuest, int reward, int numberTotal, String text) {
		this.id = id ;
		this.idQuest = idQuest ;
		this.reward = reward ;
		this.numberTotal = numberTotal ;
		this.text = text ;
	}
}
